import { Component, OnInit } from '@angular/core';
import {NewsService} from '../services/news.service';
import {ActivatedRoute} from '@angular/router';
import {UtilsService} from '../services/utils.service';
import {Comment} from '../models/comment.model';
import {Story} from '../models/story.model';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss']
})
export class CommentsListComponent implements OnInit {
  public comments: Comment[] = [];
  public story: Story;

  constructor(private _newsService: NewsService,
              private _activatedRoute: ActivatedRoute,
              private _utils: UtilsService) { }

  ngOnInit(): void {
    this._newsService.getById(this._activatedRoute.snapshot.params.id).subscribe(story => {
      story.hostname = story.url ? new URL(story.url).hostname : null;
      this._utils.addAgoField(story);
      this.story = story;
      this._utils.flattenComments(story.children, 0, this.comments);
    });
  }
}
