import { Component, OnInit } from '@angular/core';
import {NewsService} from '../services/news.service';
import {Story} from '../models/story.model';
import * as moment from 'moment';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {

  public news: Story[] = [];
  public hitsPerPage = 20;
  public currentPage = 0;
  public totalItems = 0;
  public totalPages = 0;

  constructor(private _newsService: NewsService) { }

  public ngOnInit(): void {
    this._getNews(this.currentPage);
  }

  public nextPage(): void {
    this._getNews(this.currentPage+1);
  }

  public prevPage(): void {
    this._getNews(this.currentPage-1);
  }

  private _getNews(page: number): void {
    this._newsService.getFrontPageNews(page).subscribe(res => {
      this.news = res.hits.map(item => {
        item.hostname = item.url ? new URL(item.url).hostname : null;
        item.ago = moment(item.created_at).fromNow();
        return item;
      });
      this.currentPage = res.page;
      this.totalItems = res.nbHits;
      this.totalPages = res.nbPages;
    });
  }

  public getItemIndex(i: number): number {
    return this.currentPage * this.hitsPerPage + i + 1;
  }
}
