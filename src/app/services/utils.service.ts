import { Injectable } from '@angular/core';
import * as moment from 'moment';
import {Comment} from '../models/comment.model';
import {Story} from '../models/story.model';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  public flattenComments(comments: Comment[], level = 0, result = []): void {
    if (comments) {
      level++;
      comments.forEach(comment => {
        comment.level = level;
        this.addAgoField(comment);
        result.push(comment);
        this.flattenComments(comment.children, level, result);
      });
    }
  }

  public addAgoField(item: Comment | Story): Comment | Story {
    item.ago = moment(item.created_at).fromNow();
    return item;
  }
}
