import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {PaginationResult} from '../models/pagination-result.model';
import {Story} from '../models/story.model';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private _http: HttpClient) { }

  public getFrontPageNews(page: number): Observable<PaginationResult<Story>> {
    return this._http.get(`${environment.apiUrl}search`, {params: { tags: 'front_page', page: page.toString() }});
  }

  public getById(id: number): Observable<Story> {
    return this._http.get<Story>(`${environment.apiUrl}items/${id}`);
  }
}
