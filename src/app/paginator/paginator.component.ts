import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() public hitsPerPage = 20;
  @Input() public currentPage = 0;
  @Input() public totalItems = 0;
  @Input() public totalPages = 0;

  @Output() onPrev = new EventEmitter<void>();
  @Output() onNext = new EventEmitter<void>();

  public from = 0;
  public to = 0;
  public isPrevEnabled = false;
  public isNextEnabled = false;

  constructor() { }

  public ngOnInit(): void {
    this._recalculateUi();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    this._recalculateUi();
  }

  public prevPage(): void {
    this.onPrev.emit();
  }

  public nextPage(): void {
    this.onNext.emit();
  }

  private _recalculateUi(): void {
    this.from = this.hitsPerPage * this.currentPage + 1;

    const to = this.hitsPerPage * (this.currentPage + 1);
    this.to = this.totalItems > to ? to : this.totalItems;

    this.isPrevEnabled = this.currentPage > 0;
    this.isNextEnabled = this.currentPage < this.totalPages - 1;
  }
}
