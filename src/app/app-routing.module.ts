import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsListComponent } from './news-list/news-list.component';
import {CommentsListComponent} from './comments-list/comments-list.component';

const routes: Routes = [
  {path: '', component: NewsListComponent},
  {path: ':id', component: CommentsListComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
