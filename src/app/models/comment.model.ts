export class Comment {
  public id: number;
  public created_at?: string;
  public author: string;
  public text: string;
  public points?: number;
  public parent_id?: number;
  public children?: Comment[];
  public level?: number;
  public ago?: string;
}
