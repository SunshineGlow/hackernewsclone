export class PaginationResult<T> {
  public hits?: T[];
  public page?: number;
  public nbHits?: number;
  public nbPages?: number;
  public hitsPerPage?: number;
  public processingTimeMS?: number;
  public query?: string;
  public params?: string;
}
