import {Comment} from './comment.model';

export class Story {
  public title: string;
  public url?: string;
  public author: string;
  public points: number;
  public text?: string;
  public comment_text?: string;
  public _tags?: string[];
  public num_comments: number;
  public objectID: string;
  public _highlightResult?: unknown;
  public hostname?: string;
  public ago?: string;
  public created_at?: string;
  public children?: Comment[];
}
